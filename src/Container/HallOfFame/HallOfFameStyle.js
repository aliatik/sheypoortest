import { StyleSheet,Dimensions } from 'react-native';
const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#eee'
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 5,
        marginLeft: 10
    },
    itemWrapper: {
        width: width,
    },
    itemContainer: {
        margin: 5,
        marginBottom: 0,
        backgroundColor: '#fff',
        borderRadius: 4
    },
    Top: {
        flexDirection: 'row-reverse',
        padding: 5
    },
    TopText: {
        flexGrow: 2,
        flexShrink: 12,
        flexWrap: "wrap",
    },
    price: {
        textAlign: 'right'
    },
    loader: {
        marginTop: 30,
        alignSelf: 'center'
    },
    bottom: {
        borderColor: '#eee',
        borderTopWidth: 1,
        borderStyle: 'solid',
        flexDirection: 'row-reverse',
        flex: 1
    },
    actionWrapper: {
        padding: 10,
        borderColor: '#eee',
        borderStyle: 'solid',
        borderRightWidth: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionTxt: {
        color: 'rgb(23, 135, 251)'
    },
    Icon: {
        marginLeft: 4,
        color: 'rgb(23, 135, 251)'
    }

});

export default styles;
