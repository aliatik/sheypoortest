/**
 * Hall of Fame container render a flat list that shows 5 photos and the third one is always sheldon cooper
 */

import React from 'react';
import {
    View,
    FlatList,
    Image,
    Text,
    ActivityIndicator
} from 'react-native';
import styles from './HallOfFameStyle';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather'
import {connect} from "react-redux";
import { load } from "../../Redux/Modules/filmImage";
import { baseImage } from '../../Config';
import { persianNumber, commaSeprator } from '../../Utils/persian'

class HallOfFame extends React.Component {

    static propTypes = {
        fetchListConnect: PropTypes.func.isRequired,
        loading: PropTypes.bool.isRequired,
        loaded: PropTypes.bool.isRequired,
        list: PropTypes.arrayOf(PropTypes.any).isRequired,
    };

    /**
     * fetch the new list in componentDidMount, if the array has been changed shows new images
     */

    componentDidMount(): void {
        const { fetchListConnect } = this.props;
        fetchListConnect();
    };

    /**
     * each item of flat list is shown by fastimage that can caches the images in both android and ios
     */
    renderImage = (item) => {
        return (
            <View style={styles.itemWrapper}>
                <View style={styles.itemContainer}>
                    <View style={styles.Top}>
                        <Image
                            style={styles.image}
                            source={{
                                uri: `${baseImage}${item.item.poster_path}`,
                            }}

                        />
                        <View style={{flex: 1}}>
                            <Text style={styles.TopText}>{item.item.title}</Text>
                            <Text style={styles.price}>{persianNumber(commaSeprator(item.item.vote_count * 100))} تومان</Text>
                        </View>
                    </View>
                    <View style={styles.bottom}>
                        <View style={styles.actionWrapper}>
                            <Text style={styles.actionTxt}>حذف</Text>
                            <Icon name={'trash'} size={20}  style={styles.Icon} />
                        </View>
                        <View style={styles.actionWrapper}>
                            <Text style={styles.actionTxt}>ویرایش</Text>
                            <Icon name={'edit-2'} size={20} style={styles.Icon} />
                        </View>
                        <View style={styles.actionWrapper}>
                            <Text style={styles.actionTxt}>افزایش بازدید</Text>
                            <Icon name={'trending-up'} size={20}  style={styles.Icon} />

                        </View>
                    </View>
                </View>

            </View>
        );
    };

    render() {
        const { list, loading, fetchListConnect, loaded } = this.props;
        return (
            <View style={styles.container}>
                {/* flat list */}
                {!loaded && loading
                    ? <ActivityIndicator style={styles.loader} />
                    : <FlatList
                        onRefresh={()=> {fetchListConnect()}}
                        refreshing={loading}
                        data={list}
                        renderItem={(item) => {
                            return this.renderImage(item);
                        }}/>
                }

            </View>
        )
    }
}

/**
 * Hall of Fame container render a flat list that shows 5 photos and the third one is always sheldon cooper
 */

export default connect(state => ({
    list: state.filmImage.result,
    loading: state.filmImage.loading,
    loaded: state.filmImage.loaded

}), {
    fetchListConnect: load
})(HallOfFame);
