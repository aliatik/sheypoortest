/**
 * fetch images from api and make new array where third index is sheldon cooper
 */


import { call, put, cps, select, take } from "redux-saga/effects";

export const LOAD = "sheypoor/film/LOAD";
export const LOAD_SUCCESS = "sheypoor/film/LOAD_SUCCESS";
export const LOAD_FAILURE = "sheypoor/film/LOAD_FAILURE";

const initialState = {
    result: [],
    loading: false,
    loaded: false,
    error: ""
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOAD:
            return {
                ...state,
                loading: true
            };
        case LOAD_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case LOAD_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                result: action.result
            };
        default:
            return state;
    }
}

export function load() {
    return {
        type: LOAD,
    };
}

export function loadSuccess(result) {
    return {
        type: LOAD_SUCCESS,
        result
    };
}
export function loadFailure(error) {
    return {
        type: LOAD_FAILURE,
        error
    };
}

export function* watchLoad(client) {
    try {
        const res =yield client.get('3/discover/movie?sort_by=popularity.desc&api_key=a465e2b5ca5ba1c81a34afc2c88e4f73');
        yield put(loadSuccess(res.results));
    } catch (error) {
        yield put(loadFailure())
    }
}
