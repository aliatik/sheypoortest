/**
 * startup action
 */
import NavigationService from "../../Navigator/NavigationService";
export const REHYDRATE_SUCCESS = "sheypoor/rehydrate/REHYDRATE_SUCCESS";
export const START_REHYDRATE = "sheypoor/rehydrate/START_REHYDRATE";

export function startRehydrate(error) {
  return {
    type: START_REHYDRATE,
    error
  };
}
export function rehydrateSuccess() {
  return {
    type: REHYDRATE_SUCCESS
  };
}

export function* watchRehydrate(store, client, action) {
  const persist = store.getState();
  console.log(persist)
  if (
    persist &&
    persist.auth &&
    persist.auth.skippedIntro
  ) {
    NavigationService.navigate("tab");
  } else {
    NavigationService.navigate("Intro");
  }
  yield store.dispatch(rehydrateSuccess());
}
