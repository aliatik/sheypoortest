import React, {Component} from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {
    Intro,
} from '../Container';

/**
 * app navigator: StackNavigator with 3 screens
 *
 */

import Tab from './TabNavigator'

const NavigationStack = createSwitchNavigator({
    Intro: {
        screen: Intro,
    },
    tab: {
        screen: Tab,
    },
}, {gesturesEnabled: true,});
export default createAppContainer(NavigationStack);
