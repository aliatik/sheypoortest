import React, { Component } from 'react';
import NavigationStack from './NavigationStack';
import { connect } from 'react-redux';
import  PropTypes  from 'prop-types';
import { startRehydrate } from '../Redux/Modules/rehydrate'
import NavigationService from '../Navigator/NavigationService'


class AppNavigator extends Component {


    componentDidMount() {
        this.props.startup();
    };


    render() {
        return (
                <NavigationStack
                    ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }}
                />
        );
    }
}

AppNavigator.propTypes = {
    startup: PropTypes.func,
};

const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps,
    {
        startup: startRehydrate
    }
)(AppNavigator);

