import React, {Component} from 'react';
import {Platform, View, Text, Image} from 'react-native';
import {createStackNavigator, createMaterialTopTabNavigator, createBottomTabNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather'

import {
    Welcome,
    HallOfFame,
} from '../Container';


const TabNavigator = createMaterialTopTabNavigator({
    Home: {
        screen: HallOfFame,
        navigationOptions: {
            tabBarLabel: 'همه',
        },
    },
    second: {
        screen: Welcome,
        navigationOptions: {
            tabBarLabel: 'فعال',
        },
    },
    third: {
        screen: Welcome,
        navigationOptions: {
            tabBarLabel: 'غیرفعال',
        },
    },
}, {
    initialRouteName: 'Home',
    order: ['third', 'second', 'Home'],
    lazy: true,
    animationEnabled: true,
    // optimizationsEnabled: true,
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: "#999",
        indicatorStyle: {
            backgroundColor: 'blue'
        },
        labelStyle: {
            fontSize: 10,
        },
        tabStyle: {
        },
        style: {
            backgroundColor: 'white',
        },
    }
    // headerMode: Platform.OS === 'ios' ? 'screen' : 'none',
});
/**
 * we should add component connected to redux for headerTitle but for now we consider it static,
 * and use fastimage module here for this image
 */

const tabConatiner = createStackNavigator({
    Home: {
        screen: TabNavigator,
        navigationOptions: ({ navigation }) => ({
            headerTitle: (
                <View style={{flexDirection: 'row-reverse', alignItems: 'center', width: '100%'}}>
                    <Image
                        source={{uri: 'https://avatars0.githubusercontent.com/u/279927?s=460&v=4'}}
                        style={{width: 30, height: 30, borderRadius: 15, marginLeft:7}}
                    />
                    <Text>
                        احمد رضا
                    </Text>

                </View>
            ),
            headerRight:  <Icon name={'menu'} size={20} color={'#333'} style={{marginRight: 10}} />,
            headerLeft: <Icon name={'settings'} size={20} color={'#333'} style={{marginLeft: 10}} />,
            headerStyle: { backgroundColor: '#fff', elevation:0,     borderBottomWidth: 0,
            }


        }),
    },

});



export default tabConatiner;
